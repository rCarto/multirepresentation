---
title: "Pics et dorsales démographiques"
output: 
  html_document: 
    number_sections: no
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, results = FALSE, cache = TRUE)
```


# {.tabset}

## Code

```{r, eval = FALSE}
# remotes::install_github("riatelab/potential")
library(sf)
library(raster)
library(cartography)
library(potential)

# Metadata
file <- "fig/fig17.svg"
title <- "Dorsales et pics démographiques"
authors <- "Lambert N., Giraud T., Ysebaert R., UMS RIATE, 2020"
sources <- "Gridded Population of the World, Version 4 (GPWv4), Center for International Earth Science Information Network - CIESIN - Columbia University"

# Data Import
countries <- st_read("data/countries.geojson")
graticule <- st_read("data/ne_50m_graticules_30.shp")
r <- raster("data/gpw_v4_population_count_adjusted_to_2015_unwpp_country_totals_rev11_2020_30_min.tif")

# Data Handling
crs <- "+proj=eqc +lat_ts=0 +lat_0=0 +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs"
countries <- countries[countries$id != "ATA",]
countries <- st_transform(countries,crs)
graticule <- st_transform(graticule,crs)

fact <- 1
dots <- aggregate(r, fact=fact, fun=sum)
dots <- as(dots, 'SpatialPointsDataFrame')
dots <- st_as_sf(dots)
dots <- st_transform(dots, crs)
colnames(dots) <- c("pop2020","geometry")
dots$id <- paste0("X",row.names(dots))

# Potential calculation
# Potential combination 250 k / 500 k
bbox <- st_as_sfc(st_bbox(dots, crs = crs))
grid <- create_grid(x = bbox, res = 100000)

# 1 - Rapports de potentiel 
# Potential calculation 
pot100 <- mcpotential(x = dots, y = grid, var = "pop2020", fun = "e", beta = 2, 
                      span = 100000, limit = 2000000, ncl = 3)
pot500 <- mcpotential(x = dots, y = grid, var = "pop2020", fun = "e", beta = 2, 
                      span = 500000, limit = 2000000, ncl = 3)

# Population potential ratio (dist min / dist max)
grid$pot100 <- pot100
grid$pot500 <- pot500
grid$pot_100_500 <- grid$pot100 / grid$pot500 

# Cartography parameters
brks <- c(0,0.01, 0.015,0.02,0.03,0.04,0.05,0.06,0.07,0.08,0.09,0.1,0.2, 1)
cols <- hcl.colors(13, palette = "viridis", alpha = NULL, rev = FALSE, fixup = TRUE)
pot_iso <- equipotential(x = grid, var = "pot_100_500", breaks = brks,
                    mask = countries)

# Display
svg(file,width=12,height=8)
ghostLayer(countries, bg = "lightblue")
plot(st_geometry(countries), lwd = 0.3, col = "#d4cbae", border = NA, add= TRUE)
choroLayer(x = pot_iso,
           var = "center",
           breaks = brks,
           col = cols,
           border = NA,
           legend.pos = "n",
           add = TRUE)
layoutLayer(title = title,
            author =  authors,
            sources = sources,
            frame = TRUE,
            scale = FALSE,
            theme = "sand.pal")
dev.off()
```



## Sortie brute

![](fig/fig17.svg)

## Haute résolution

![](fig/figure17HR.png)

## Version article

![](fig/figure17.png)