---
title: "Distorsion démographique"
output: 
  html_document: 
    number_sections: no
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE, results = FALSE, cache = TRUE)
```


# {.tabset}

## Code

La préparation des données s'est faite avec R (production de 2 couches géographique.).  
L'anamorphose a été réalisée avec le logiciel [ScapeToad](http://scapetoad.choros.place/).

```{r, eval=FALSE}
library(sf)
library(raster)
library(cartography)
library(rnaturalearth)

# Data Import
countries <- ne_download(scale = "medium", type = "countries", returnclass = "sf")
countries <- countries[countries$SOV_A3 != "ATA",]
r <- raster("data/gpw_v4_population_count_adjusted_to_2015_unwpp_country_totals_rev11_2020_30_min.tif")

# Data Handling
crs <- "+proj=eqc +lat_ts=0 +lat_0=0 +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs"
countries <- st_transform(countries,crs)

# Computation
dots <- st_as_sf(as(r, 'SpatialPointsDataFrame'))
dots <- st_transform(dots, crs)
colnames(dots) <- c("pop2020","geometry")
grid <- st_make_grid(dots, cellsize = 50000, square = TRUE)
grid <- aggregate(dots["pop2020"], grid, sum)
grid$tmp <- grid$pop2020
grid$tmp[grid$tmp == 0] <- 1
grid$pop2020inv <- 1/grid$tmp

st_write(grid,"data/layer1.shp")
st_write(countries,"data/layer2.shp")
```


## Haute résolution

![](fig/figure8HR.png)

## Version article

![](fig/figure8.png)